FROM node:10.15.3-alpine as build

ARG REACT_APP_API_URL

WORKDIR /app

COPY . /app

RUN echo $REACT_APP_API_URL

RUN yarn install

RUN yarn run build --max_old_space_size=4096

# production environment
FROM nginx:1.16.0-alpine

COPY --from=build /app/build /usr/share/nginx/html

COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
