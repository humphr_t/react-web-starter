var mkdirpSync = require("mkdirp").sync;
var fs = require("fs");
var glob = require("glob");
var parser = require("typescript-react-intl").default;

const pattern = "src/**/*.tsx";
const messagesFile = "src/locales/en.json";

mkdirpSync("src/locales");

function runner(pattern, cb) {
  var results = [];
  pattern = pattern || "src/**/*.@(tsx|ts)";
  glob(pattern, function(err, files) {
    if (err) {
      throw new Error(err);
    }
    files.forEach(f => {
      var contents = fs.readFileSync(f).toString();
      var res = parser(contents);
      results = results.concat(res);
    });

    cb && cb(results);
  });
}

runner("src/**/*.@(tsx|ts)", function(res) {
  var locale = {};

  res.forEach(r => {
    locale[r.id] = r.defaultMessage;
  });

  // save file to disk。you can save as a json file,just change the ext and contents as you want.
  fs.writeFileSync(messagesFile, `${JSON.stringify(locale, null, 2)}\r`);
});
