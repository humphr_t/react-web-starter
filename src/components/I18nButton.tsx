import React, { Component } from "react";
import { connect } from "react-redux";
import { updateLocale } from "@state/actions/i18n";
import { HeaderItem, MenuItem } from "./HeaderItem";
import { Menu, Icon, Row } from "antd";

interface Props {
  locale: string;
  onLocaleChange(local: string): any;
  theme: "light" | "dark";
}

class I18nButton extends Component<Props> {
  render() {
    const menu = (
      <Menu>
        <MenuItem key="0">
          <span onClick={() => this.props.onLocaleChange("fr")}>FR</span>
        </MenuItem>
        <Menu.Divider />
        <MenuItem key="1">
          <span onClick={() => this.props.onLocaleChange("en")}>EN</span>
        </MenuItem>
      </Menu>
    );

    return (
      <HeaderItem overlay={menu} trigger={["hover"]}>
        <Row justify="center" align="middle">
          <Icon
            type="global"
            style={{
              fontSize: 16,
              verticalAlign: "middle",
              color: this.props.theme === "dark" ? "#fff" : "inehrit"
            }}
          />
        </Row>
      </HeaderItem>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    locale: state.i18n.locale
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onLocaleChange: updateLocale({ dispatch })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(I18nButton);
