import React, { Component } from "react";
import { Button, Icon, Spin } from "antd";
import { FormattedMessage } from "react-intl";
import { I18nProps } from "@models/i18n";
import { ButtonProps } from "antd/lib/button";

interface IProps extends ButtonProps {
  onSubmit(data: any): void;
  loading: boolean;
  i18n: I18nProps;
}

export default class SubmitButton extends Component<IProps> {
  render() {
    const {
      onSubmit,
      loading,
      i18n: { id, defaultMessage },
      ...rest
    } = this.props;

    return (
      <Button
        type="primary"
        htmlType="submit"
        onClick={onSubmit}
        disabled={loading}
        {...rest}
      >
        <FormattedMessage id={id} defaultMessage={defaultMessage} />

        {loading && (
          <Spin
            indicator={
              <Icon
                type="loading"
                style={{ fontSize: 16, marginLeft: 2 }}
                spin
              />
            }
          />
        )}
      </Button>
    );
  }
}
