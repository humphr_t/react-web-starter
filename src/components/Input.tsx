import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Input as AntInput, Form } from "antd";
import { InputProps as AntInputProps } from "antd/lib/input";
import { FormikTouched, FormikErrors } from "formik";
import { I18nProps } from "@models/i18n";

interface InputProps extends AntInputProps {
  i18n?: I18nProps | undefined;
}

export class Input extends Component<InputProps> {
  render() {
    const { i18n, ...rest } = this.props;

    if (i18n) {
      const { id, defaultMessage } = i18n as I18nProps;

      return (
        <FormattedMessage id={id} defaultMessage={defaultMessage}>
          {msg => <AntInput {...rest} placeholder={msg.toString()} />}
        </FormattedMessage>
      );
    } else {
      return <AntInput {...rest} />;
    }
  }
}

interface InputItemProps extends InputProps {
  touched: FormikTouched<any>;
  errors: FormikErrors<any>;
  name: string;
  label?: string;
}

export class InputItem extends Component<InputItemProps> {
  render() {
    const { touched, errors, label, ...rest } = this.props;

    return (
      <Form.Item
        label={label}
        help={
          touched[rest.name] && errors[rest.name] ? (
            <FormattedMessage
              id={errors[rest.name] as string}
              defaultMessage="Field error"
            />
          ) : (
            ""
          )
        }
        validateStatus={
          touched[rest.name] && errors[rest.name] ? "error" : undefined
        }
      >
        <Input {...rest} />
      </Form.Item>
    );
  }
}

export default Input;
