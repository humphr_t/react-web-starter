import React, { Component } from "react";
import { Layout, Row, Col, Typography } from "antd";
import styled from "styled-components";
import I18nButton from "./I18nButton";
import ReactIcon from "@assets/react-icon.svg";

const { Header } = Layout;
const { Title } = Typography;

const StyledHeader = styled(Header)`
  background: #001529;
  padding: 0;
  box-shadow: 0 1px 4px rgba(0, 21, 41, 0.08);
`;

export default class HeaderComponent extends Component {
  render() {
    return (
      <StyledHeader>
        <Row
          type="flex"
          justify="space-between"
          align="middle"
          style={{ height: "100%" }}
        >
          <Col>
            <Row
              type="flex"
              justify="start"
              align="middle"
              style={{ height: "100%" }}
            >
              <img className="logo" src={ReactIcon} width={64} alt="Logo" />

              <Title level={3} style={{ color: "#fff", marginBottom: 0 }}>
                React Web Starter
              </Title>
            </Row>
          </Col>

          <I18nButton theme="dark" />
        </Row>
      </StyledHeader>
    );
  }
}
