import { Dropdown, Menu } from "antd";
import styled from "styled-components";

export const MenuItem = styled(Menu.Item)`
  &:hover {
    background-color: rgba(3, 216, 255, 0.25);
  }
`;

export const HeaderItem = styled(Dropdown)`
  cursor: pointer;
  padding: 0 20px;
  height: 100%;
  &:hover {
    background-color: rgba(0, 0, 0, 0.025);
  }
`;

export default HeaderItem;
