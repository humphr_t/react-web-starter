import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { IAppState } from "@state/reducer";

const AuthRoute = ({ component: Component, type, ...rest }: any) => (
  <Route
    {...rest}
    render={props => {
      if (rest.isAuth) {
        return <Component {...props} />;
      } else {
        return <Redirect to="/login" />;
      }
    }}
  />
);

export default connect((state: IAppState) => ({
  isAuth: state.auth.isAuth
}))(AuthRoute);
