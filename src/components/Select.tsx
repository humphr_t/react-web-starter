import React, { Component } from "react";
import {
  FormikProps,
  withFormik,
  Field as FormikField,
  FormikValues
} from "formik";
import {
  Input,
  Row,
  Button,
  Select as AntSelect,
  Switch,
  Form as AntForm,
  Form
} from "antd";
import { SelectProps } from "antd/lib/select";

interface IProps extends SelectProps {
  name: string;
  value: any;
  onSetFieldValue(field: string, value: any): void;
  onSetFieldTouched(field: string, isTouched?: boolean | undefined): void;
}

export class Select extends Component<IProps> {
  render() {
    const {
      name,
      value,
      onSetFieldTouched,
      onSetFieldValue,
      children,
      ...rest
    } = this.props;

    return (
      <FormikField
        name={name}
        render={(data: any) => (
          <AntSelect
            {...data.field}
            onChange={val => onSetFieldValue(name, val)}
            onBlur={() => onSetFieldTouched(name, true)}
            value={value}
            {...rest}
          >
            {children}
          </AntSelect>
        )}
      />
    );
  }
}

interface ItemProps extends IProps {
  label?: string;
}

export class SelectItem extends Component<ItemProps> {
  render() {
    const { label, ...rest } = this.props;

    return (
      <Form.Item label={label}>
        <Select {...rest} />
      </Form.Item>
    );
  }
}

export default Select;
