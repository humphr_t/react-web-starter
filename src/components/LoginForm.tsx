import React, { Component } from "react";
import { Row, Icon } from "antd";
import { withFormik, FormikProps } from "formik";
import * as Yup from "yup";
import SubmitButton from "./SubmitButton";
import { InputItem } from "./Input";

export interface FormValues {
  username: string;
  password: string;
}

interface FormProps {
  onSubmit(form: FormValues): any;
  loading: boolean;
}

class FormComponent extends Component<FormProps & FormikProps<FormValues>> {
  render() {
    const {
      values,
      handleChange,
      handleBlur,
      handleSubmit,
      loading,
      touched,
      errors
    } = this.props;

    return (
      <form>
        <InputItem
          name="username"
          i18n={{
            id: "login.form.placeholder.username",
            defaultMessage: "Username"
          }}
          prefix={<Icon type="user" style={{ color: "rgba(0, 0, 0, .25)" }} />}
          touched={touched}
          errors={errors}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.username}
        />

        <InputItem
          name="password"
          type="password"
          i18n={{
            id: "login.form.placeholder.password",
            defaultMessage: "Username"
          }}
          prefix={<Icon type="lock" style={{ color: "rgba(0, 0, 0, .25)" }} />}
          touched={touched}
          errors={errors}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.password}
        />

        <Row type="flex" justify="end">
          <SubmitButton
            onSubmit={handleSubmit}
            loading={loading}
            i18n={{ id: "login.form.submit", defaultMessage: "Sign In" }}
          />
        </Row>
      </form>
    );
  }
}

export const LoginForm = withFormik<FormProps, FormValues>({
  mapPropsToValues: props => ({
    username: "",
    password: ""
  }),

  validationSchema: Yup.object().shape({
    username: Yup.string().required("error.login.form.username.required"),
    password: Yup.string().required("error.login.form.password.required")
  }),

  handleSubmit(values: FormValues, { props }) {
    props.onSubmit(values);
  }
})(FormComponent);

export default LoginForm;
