export interface I18nProps {
  id: string;
  defaultMessage: string;
}
