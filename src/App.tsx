/* eslint-disable */
import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Main from "@pages/app/Main";
import Login from "@pages/Login";
import NotFound from "@pages/errors/404";
import { logout } from "@state/actions/auth";
import AuthRoute from "@components/AuthRoute";

interface IProps {
  logout(): void;
}

class App extends Component<IProps> {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <AuthRoute exact path="/app/:page" component={Main} />
          <Route exact path="/404" component={NotFound} />
          <Route exact path="/login" component={Login} />
          <Route
            exact
            path="/logout"
            component={() => {
              this.props.logout();
              return <Redirect to="/login" />;
            }}
          />
          <Route exact path="/" component={() => <Redirect to="/app/home" />} />
        </Switch>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = {
  logout
};

export default connect(
  null,
  mapDispatchToProps
)(App);
