import { LOGIN, LOGOUT, LOGIN_SUCCESS } from "@state/reducers/auth";
import { store } from "@state/store";

export function login(username: string, password: string) {
  setTimeout(() => {
    store.dispatch({ type: LOGIN_SUCCESS });
  }, 2000);

  return {
    type: LOGIN
    // payload: {
    //   request: {
    //     method: "POST",
    //     url: "/api/login",
    //     data: {
    //       username: username,
    //       password: password
    //     }
    //   }
    // }
  };
}

export function logout(): any {
  return {
    type: LOGOUT
  };
}
