import { locales, en } from "@locales/index";

export const LOCALE_CHANGE = "LOCALE_CHANGE";

export interface Ii18nState {
  locale: string;
  messages: any;
}

const initialState: Ii18nState = {
  locale: "en",
  messages: en
};

export default function i18nReducer(
  state: Ii18nState = initialState,
  action: any
) {
  const ret =
    action.type === LOCALE_CHANGE
      ? {
          locale: action.payload,
          messages: locales[action.payload] || en
        }
      : state;

  return ret;
}
