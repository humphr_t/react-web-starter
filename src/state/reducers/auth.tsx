import React from "react";
import storage from "redux-persist/lib/storage/session";
import { AxiosResponse } from "axios";
import { FormattedMessage } from "react-intl";

export const LOGIN = "LOGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";

export const LOGOUT = "LOGOUT";

export const authPersistConfig = {
  key: "auth",
  storage: storage
};

export interface IAuthState {
  loading: boolean;
  isAuth: boolean;
  error: any;
}

const initialState: IAuthState = {
  loading: false,
  isAuth: false,
  error: undefined
};

function createErrorMessage(error: any): any {
  const props = {
    id: "error.default",
    defaultMessage: "An error occured"
  };

  if (error.status !== undefined) {
    if (error.data === "Network Error") {
      props.id = "error.network";
      props.defaultMessage = "Not connected to internet";
    }
  } else {
    if (error.response.status === 404) {
      props.id = "error.auth.incorrect";
      props.defaultMessage = "Username or password incorrect";
    }
  }
  return <FormattedMessage {...props} />;
}

export default function authReducer(
  state: IAuthState = initialState,
  action: { type: string; payload: AxiosResponse; error: any }
): IAuthState {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loading: true,
        isAuth: false,
        error: undefined
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        isAuth: true,
        error: undefined
      };

    case LOGIN_FAIL:
      return {
        ...state,
        loading: false,
        isAuth: false,
        error: createErrorMessage(action.error)
      };

    case LOGOUT:
      return initialState;

    default:
      return state;
  }
}
