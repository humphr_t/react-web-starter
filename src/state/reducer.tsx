import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";

import authReducer, {
  IAuthState,
  authPersistConfig
} from "@state/reducers/auth";
import i18nReducer, { Ii18nState } from "@state/reducers/i18n";

export interface IAppState {
  auth: IAuthState;
  i18n: Ii18nState;
}

export default combineReducers({
  auth: persistReducer(authPersistConfig, authReducer),
  i18n: i18nReducer
});
