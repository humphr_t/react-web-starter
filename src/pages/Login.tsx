import React, { Component } from "react";
import { connect } from "react-redux";
import { login } from "@state/actions/auth";
import { IAuthState } from "@state/reducers/auth";
import { Redirect } from "react-router";
import { FormattedMessage, IntlShape } from "react-intl";
import { Card, Row, Col, Layout, message } from "antd";
import { IAppState } from "@state/reducer";
import { FormValues, LoginForm } from "@components/LoginForm";
import Header from "@components/Header";

const { Content } = Layout;

interface ILoginProps extends IAuthState {
  intl: IntlShape;
  login(username: string, password: string): any;
}

const styles = {
  title: {
    display: "flex",
    justifyContent: "center"
  },
  form: {
    maxWidth: "400px",
    width: "100%"
  }
};

class Login extends Component<ILoginProps> {
  constructor(props: ILoginProps) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(form: FormValues) {
    this.props.login(form.username, form.password);
  }

  componentDidUpdate() {
    if (this.props.error) {
      message.error(this.props.error, 3);
    }
  }

  render() {
    if (this.props.isAuth) {
      return <Redirect to="/app/home" />;
    }

    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Header />
        <Content>
          <Row type="flex" justify="center" style={{ marginTop: "2em" }}>
            <Col span={22}>
              <Row
                type="flex"
                justify="center"
                style={{ flexDirection: "column" }}
                align="middle"
              >
                <h1 style={styles.title}>
                  <FormattedMessage id="login.title" defaultMessage="Login" />
                </h1>
                <Card style={styles.form}>
                  <LoginForm
                    onSubmit={this.handleSubmit}
                    loading={this.props.loading}
                  />
                </Card>
              </Row>
            </Col>
          </Row>
        </Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state: IAppState) => {
  return state.auth;
};

const mapDispatchToProps = {
  login
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
