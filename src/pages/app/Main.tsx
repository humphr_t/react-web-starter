import React, { Component } from "react";
import { Layout } from "antd";
import Home from "@pages/app/Home";
import { Route, Switch } from "react-router-dom";
import Header from "@components/Header";

const { Content } = Layout;

class Main extends Component {
  render() {
    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Header />
        <Content
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280
          }}
        >
          <Switch>
            <Route exact path="/app/home" component={Home} />
          </Switch>
        </Content>
      </Layout>
    );
  }
}

export default Main;
