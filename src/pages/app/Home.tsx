import React, { Component } from "react";
import { Button, Row } from "antd";
import { Link } from "react-router-dom";

export default class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <h1>Home Component</h1>
        <Row>
          <strong>Successfully connected !</strong>
        </Row>
        <Row>
          <Button type="primary">
            <Link to="/logout">Sign Out</Link>
          </Button>
        </Row>
      </React.Fragment>
    );
  }
}
